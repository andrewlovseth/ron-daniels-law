function removeHash () { 
    history.pushState("", document.title, window.location.pathname + window.location.search);
}


$(document).ready(function() {

	// rel="external"
	$('a[rel="external"]').click( function() {
		window.open( $(this).attr('href') );
		return false;
	});

	// Menu Toggle
	$('#toggle').click(function(){

		$('header').toggleClass('open');
		$('nav').slideToggle(300);
		return false;
	});


	// FitVids
	$('.embed').fitVids();

	// SmoothScroll
	$('nav a').smoothScroll();


	if(location.hash) {
		var hash = location.hash;
		var targetVar = 'nav a[href^="/' + hash + '"]';
		$(targetVar).trigger('click');
		removeHash();
	}

});
