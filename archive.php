<?php get_header(); ?>

	<section id="main">
		<div class="wrapper">


			<section id="blog-header">

				<div class="header">
					<em>From the desk of</em>
					<h2>Ron Daniels</h2>
				</div>
				<div class="photo">
					<img src="<?php $image = get_field('blog_header_image', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

			</section>


			<section id="blog">

				<?php echo do_shortcode('[ajax_load_more post_type="post" posts_per_page="8" scroll="false" transition="fade"]'); ?>


			</section>


		</div>
	</section>
	
<?php get_footer(); ?>