<!DOCTYPE html>
<html>
<head>

	<title><?php wp_title(); ?></title>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<link href="https://fonts.googleapis.com/css?family=Libre+Baskerville:400,400i,700|Yantramanav:400,500,900" rel="stylesheet">
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
	
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

	<section id="mobile-contact-header">
		<div class="wrapper">
			<h5 class="tel"><?php the_field('phone', 'options'); ?></h5>

			<h5 class="email"><a href="mailto:<?php the_field('email', 'options'); ?>"><?php the_field('email', 'options'); ?></a></h5>
		</div>
	</section>

	<header>
		<div class="wrapper">

			<h5 class="tel"><?php the_field('phone', 'options'); ?></h5>

			<div id="header-logo">
				<a href="<?php echo site_url('/'); ?>"><img src="<?php $image = get_field('header_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
			</div>

			<h5 class="email"><a href="mailto:<?php the_field('email', 'options'); ?>"><?php the_field('email', 'options'); ?></a></h5>

			<a href="#" id="toggle" class="no-translate">
				<div class="patty"></div>
			</a>

		</div>
	</header>

	<nav>
		<div class="wrapper">
			
			<?php if(have_rows('navigation', 'options')): while(have_rows('navigation', 'options')): the_row(); ?>
			 
			    <a href="<?php the_sub_field('link'); ?>">
			        <?php the_sub_field('label'); ?>
			    </a>

			<?php endwhile; endif; ?>

		</div>
	</nav>