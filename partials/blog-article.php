<article>
	<div class="featured-image">
		<img src="<?php $image = get_field('featured_image'); echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
	</div>

	<div class="info">
		<span class="date"><?php the_date('n/j/y'); ?></span>
		<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		<?php the_field('teaser'); ?>
	</div>
</article>