<?php

/*

	Template Name: Home

*/

get_header(); ?>

	<section id="main">

		<section id="hero" class="cover" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">
			<div class="wrapper">

				<h1><span><?php the_field('hero_headline'); ?></span></h1>
				<h3><span><?php the_field('hero_sub_headline'); ?></span></h3>
				<h5 class="tel">
					<span class="label">Call or Text Us Today</span><br/>
					<span class="phone"><?php the_field('phone', 'options'); ?></span>
				</h5>

			</div>
		</section>

		<section id="areas-of-practice">
			<div class="wrapper">

				<div class="headline">
					<h3>Areas of Practice</h3>
				</div>

				<div class="list">
					
					<?php if(have_rows('hero_areas_of_practice')): while(have_rows('hero_areas_of_practice')): the_row(); ?>
	 
					    <div class="item">
					        <p><?php the_sub_field('practice'); ?></p>
					    </div>

					<?php endwhile; endif; ?>

				</div>
				


			</div>
		</section>

		<section id="your-rights" class="subsection white">
			<div class="wrapper">

				<h2><?php the_field('your_rights_headline'); ?></h2>

				<div class="content fdcpa">
					<div class="header">
						<h3><?php the_field('fdcpa_shortname'); ?></h3>
						<h4><?php the_field('fdcpa_full_name'); ?></h4>
					</div>

					<div class="info">
						<?php the_field('fdcpa_info'); ?>
					</div>
				</div>

				<div class="content fcra">
					<div class="header">
						<h3><?php the_field('fcra_shortname'); ?></h3>
						<h4><?php the_field('fcra_full_name'); ?></h4>
					</div>

					<div class="info">
						<?php the_field('fcra_info'); ?>
					</div>
				</div>

				<div class="content personal-injury">
					<div class="header">
						<h3><?php the_field('personal_injury_full_name'); ?></h3>
					</div>

					<div class="info">
						<?php the_field('personal_injury_info'); ?>
					</div>
				</div>


			</div>
		</section>

		<section id="my-practice" class="subsection gray">
			<div class="wrapper">

				<h2><?php the_field('my_practice_headline'); ?></h2>

				<div class="content">

					<div class="info">
						<?php the_field('my_practice_info'); ?>
					</div>

					<div class="outcomes">
						<?php if(have_rows('outcomes')): while(have_rows('outcomes')): the_row(); ?>
						 
						    <div class="outcome">
						    	<h3><?php the_sub_field('amount'); ?></h3>
						        <?php the_sub_field('description'); ?>
						    </div>

						<?php endwhile; endif; ?>
					</div>

					<div class="testimonials">
						<?php if(have_rows('testimonials')): while(have_rows('testimonials')): the_row(); ?>

							<div class="testimonial">
							    <blockquote>
							        <?php the_sub_field('quote'); ?>
							    </blockquote>
							    <cite>&mdash; <?php the_sub_field('source'); ?></cite>
							</div>

						<?php endwhile; endif; ?>
					</div>

				</div>

			</div>
		</section>


		<section id="what-it-costs" class="subsection white">
			<div class="wrapper">

				<h2><?php the_field('what_it_costs_headline'); ?></h2>

				<div class="content">

					<div class="info">
						<?php the_field('what_it_costs_info'); ?>
					</div>

					<div class="header">
						<h3><?php the_field('guarantee_headline'); ?></h3>
						<h4><?php the_field('guarantee_sub_headline'); ?></h4>
						<h5 class="tel"><span><?php the_field('phone', 'options'); ?></span></h5>
					</div>

					<div class="note">
						<p><?php the_field('what_it_costs_note'); ?></p>
					</div>

				</div>

			</div>
		</section>


		<section id="contact" class="subsection red">
			<div class="wrapper">

				<h2><?php the_field('contact_headline'); ?></h2>

				<div class="content">

					<div class="social">
						<h3><?php the_field('contact_tagline'); ?></h3>

						<div class="links">
							<h4>Connect Socially</h4>
							<a href="<?php the_field('facebook', 'options'); ?>" class="ir facebook" rel="external">Facebook</a>
							<a href="<?php the_field('linkedin', 'options'); ?>" class="ir linkedin" rel="external">LinkedIn</a>
						</div>

						<div class="badges">
							<?php the_field('contact_badges'); ?>
						</div>
					</div>

					<div class="contact-info">
						<h5>Phone</h5>
						<h4><?php the_field('phone', 'options'); ?></h4>

						<h5>Email</h5>
						<h4><a href="mailto:<?php the_field('email', 'options'); ?>"><?php the_field('email', 'options'); ?></a></h4>

						<h5>Address</h5>
						<h4><?php the_field('address', 'options'); ?></h4>

						<div class="map">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d26883.76003135529!2d-83.65830145768909!3d32.620304964591995!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88f3e10c0fd1be4d%3A0x14cac52d0618831a!2s723%20Bernard%20Dr%2C%20Warner%20Robins%2C%20GA%2031093!5e0!3m2!1sen!2sus!4v1567007129895!5m2!1sen!2sus" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
						</div>
					</div>

				</div>

			</div>
		</section>

	</section>

<?php get_footer(); ?>