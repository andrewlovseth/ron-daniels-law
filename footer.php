	<footer>
		<div class="wrapper">

			<div class="logo">
				<a href="<?php echo site_url('/'); ?>"><img src="<?php $image = get_field('footer_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
			</div>

			<div class="nav">

				<?php if(have_rows('footer_navigation', 'options')): while(have_rows('footer_navigation', 'options')): the_row(); ?>
				 
				    <a href="<?php the_sub_field('link'); ?>">
				        <?php the_sub_field('label'); ?>
				    </a>

				<?php endwhile; endif; ?>
				
			</div>

			<div class="copyright">
				<p><?php the_field('copyright', 'options'); ?></p>
			</div>


		</div>
	</footer>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/plugins.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/site.js"></script>
	
	<?php wp_footer(); ?>

</body>
</html>