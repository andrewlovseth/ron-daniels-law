<?php get_header(); ?>
	
	<section id="featured-image" class="cover" style="background-image: url(<?php $image = get_field('featured_image'); echo $image['url']; ?>);">
	</section>

    <article>

		<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>				
    	
			<div class="article-header">
				<span class="date"><?php the_date('n/j/y'); ?></span>
				<h1><?php the_title(); ?></h1>
				<span class="author">by <?php the_author(); ?></span>
			</div>

			<div class="article-body">
				<?php the_content(); ?>
		    </div>

	    	
			<div class="article-footer">
				<div class="tags">
					 <?php the_tags( '', ' ', '' ); ?> 
				</div>

				<div class="about">
					<p><?php the_field('blog_about_message', 'options'); ?></p>
				</div>

				<div class="share">
					<h5>Share this article</h5>

					<div class="links">
						<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink()); ?>" class="facebook" rel="external"><img src="<?php echo bloginfo('template_directory'); ?>/images/share-facebook.png" alt="Facebook" /></a>
						<a href="https://twitter.com/intent/tweet/?text=<?php echo urlencode(get_the_title()); ?>&url=<?php echo urlencode(get_permalink()); ?>" class="twitter" rel="external"><img src="<?php echo bloginfo('template_directory'); ?>/images/share-twitter.png" alt="Twitter" /></a>
						<a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode(get_permalink()); ?>"><img src="<?php echo bloginfo('template_directory'); ?>/images/share-linkedin.png" alt="Twitter" /></a>
					</div>
				</div>
			</div>

		<?php endwhile; endif; ?>

    </article>

<?php get_footer(); ?>